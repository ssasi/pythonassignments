import pytest
from calculator import add,divide
def test_add():
    res=add(7,2)
    assert res==9
    assert add(9,1)==10

def test_div():
    assert divide(14,7)==2
    assert divide(10,5)==2
    # assert divide(10,3)==3.33, "rounding works"
    # assert divide(2,0)=="division by zero error"
