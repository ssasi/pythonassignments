def ispalindrome(data):
	if type(data)!=str and type(data)!=list:
		return False
	if type(data)==str:
		data=data.lower()
	reverse=data[::-1]
	if data==reverse:
		return True
	else:
		return False
if __name__=='__main__':
	print(ispalindrome('sasi'))
	print(ispalindrome('54645'))
	print(ispalindrome([1,2,3,2,1]))
	print(ispalindrome(123))