import random, string
class password:
	def generate_pwd():
		number_of_passwords = int(input("How many passwords do you want to generate? "))
		password_length = int(input("Provide the password length: "))

		characters = string.ascii_letters + string.digits + string.punctuation

		for password_index in range(number_of_passwords):
				password = ""   

				for index in range(password_length):
						password = password + random.choice(characters)

				print("Password {} generated: {}".format(password_index, password))
	def Get_Score(password):
			print("\nPassword : " + str(password))
			t = 0
			sugg = []
			uppercaseLetter = 0
			lowercaseLetter = 0
			specialCharacter = 0
			number = 0
			underscore = 0
			brackets = 0
			#Checking password length
			if len(password) > 8:
					t= t+2
			else:
					sugg.append("Length Should Be More Than 8")
			#Assigning score
			for i in password:
					if i.isdigit():
						number = 1
					if i.isupper():
							uppercaseLetter = 1
					if i.islower():
							lowercaseLetter = 1
					if i in ['!', '@', '#', '$', '%', '&']:
							specialCharacter = 2
					if i == "_":
							underscore = 1
					if i in ['(', ')', '{', '}', '[', ']', '<', '>']:
							brackets = 2
			#strong password Suggestion
			if number == 0:
					sugg.append("Add Numbers")
			if lowercaseLetter == 0:
					sugg.append("Add Lower Case Letters")
			if uppercaseLetter == 0:
					sugg.append("Add Upper Case letters")
			if specialCharacter == 0:
					sugg.append("Add Special Character ! @ # $ % &")
			if underscore == 0:
					sugg.append("Add Underscore")
			if brackets == 0:
					sugg.append("Add Brackets ( ) { } [ ] < >")
			return sugg,(t+number+uppercaseLetter+lowercaseLetter+specialCharacter+underscore+brackets)
	password = input("Enter Passwords : ")
	suggestion,score = Get_Score(password)
	if score <= 3:
			print("Strength : 50%")
	elif score <= 6 and score > 3:
			print("Strength : 75%")
	else:
			print("Strength : 95%")
	if score == 10:
			print("Status : Valid Passwords")
	else:
			print("Status : Invalid Passwords")
			print("\n\tPassword Suggestion : ")
			print(*suggestion, sep = "\n")
s=password()
s.generate_pwd()
s.Get_Score(password)
