import pytest
from palindrome import ispalindrome
def test_ispalindrome():
	assert ispalindrome("mom")==True,"palindrome Tested"
	assert ispalindrome("sister")==False,"palindrome Tested"
	assert ispalindrome([1,2,3,4])==False,"palindrome Tested"
@pytest.mark.parametrize("palindromeparams",["dad","mom","never odd or even"] )
def test_isPalindrome2(palindromeparams):
	ispalindrome(palindromeparams)
