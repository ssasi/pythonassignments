# Python program to check for balanced brackets using previously created linked list as a stack
'''
Object oriented program to implement linked list as a stack to check for balanced brackets.
The stack class Contains the following member functions.
  -> To check whether stack is empty
  -> Push an element at top
  -> Pop an element from top
  -> Display the list
'''


class Node:
    '''
    Creats a node for the elements in the lists
    ...
    Methods
    -------
    __init__(self, data)
            Initialize an instance with attribute data as data and next as
                    None
    '''
    def __init__(self, data):
        self.data = data
        self.next = None


class Stack:
    '''
    class to handling and creating linked list
    ...
    Methods
    -------
    __init__(self)
                    Initialize an Instance(Linked List as stack) with head as None

    isempty(self)
            checks if the stack is empty or not

    push(self, data)
            insert an element with data at the top of stack

    pop(self)
            delete an element with data at the top of stack

    display(self)
            prints elements of the linked list as stack in a formated form

		isbalanced(str1)
								to check whether the paranthesis is balanced or not
    '''

    # head is default NULL
    def __init__(self):
        '''
        Contructed setting head as None when object is create
        '''
        self.head = None

    # Checks if stack is empty
    def isempty(self):
        '''
        checks if the stack is empty or not
        Returns
        -------
        boolean value
        '''
        return bool(self.head is None)

    # Method to add data to the stack which will be added at the start
    def push(self, data):
        '''
        pushes element at the top of the stack
        ...
        Parameters
        ----------
        data : Any
                data that should be pushed to the stack
        '''
        if self.head is None:
            self.head = Node(data)
        else:
            newnode = Node(data)
            newnode.next = self.head
            self.head = newnode

    # Remove element that is the current head (start of the stack)
    def pop(self):
        '''
        checks if the stack is empty or not
        '''
        if self.isempty():
            return None
        # Removes the head node and makes the preceding element as the head
        poppednode = self.head
        self.head = self.head.next
        poppednode.next = None
        return poppednode.data

    # Prints the elements of stack
    def display(self):
        '''
        prints the currently stored elements in the stack

        Returns
        ------
        string
              if stack is empty
				list
              elements in the stack
        '''
        if self.isempty():
            return "Stack Underflow"
        iternode = self.head
        new_string = ''
        while iternode is not None:
            new_string += iternode.data
            iternode = iternode.next
        return new_string

    @staticmethod
    def isbalanced(str1):
        '''
        function to check whether the paranthesis are balanced or not
        Parameters
        ----------
        str1 : string

        Returns
        ------
        boolean value
                True if paranthesis are balanced
                False if paranthesis are unbalanced
        '''
        count = 0
        ans = False
        for i in str1:
            if i in ('(', '{', '['):
                count += 1
            elif i in (')', '}', ']'):
                count -= 1
            if count < 0:
                return ans
        if count == 0:
            return not ans
        return ans


# Driver code
if __name__ == "__main__":
    MyStack = Stack()

    MyStack.push(']')
    MyStack.push('}')
    MyStack.push(')')
    MyStack.push('(')
    MyStack.push(')')
    MyStack.push('(')
    MyStack.push('{')
    MyStack.push('[')

    # MyStack.pop()
    stack_data = MyStack.display()

    inp = stack_data
    print(inp, '-', MyStack.isbalanced(inp))

