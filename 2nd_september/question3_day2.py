
openbraces = ["[", "{", "("]
closingbraces = ["]", "}", ")"]

def isbalanced(inp):

    stack = []
    for i in inp:
        if i in openbraces:
            stack.append(i)
        elif i in closingbraces:
            pos = closingbraces.index(i)
            if ((len(stack) > 0) and
                    (openbraces[pos] == stack[len(stack)-1])):
                stack.pop()
            return "Unbalanced"
    if len(stack) == 0:
        return "Balanced"
    return "Unbalanced"


filename = input()

with open(filename, "r") as file:
    data = file.read()
    file.close()

input1 = str(data)
print(input1, "-", isbalanced(input1))



