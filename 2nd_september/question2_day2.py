# Python program to check for balanced brackets using previously created linked list as a stack



class Node:
   
    def __init__(self, data):
        self.data = data
        self.next = None


class Stack:
    
    def __init__(self):
       
        self.head = None

    # Checks if stack is empty
    def isempty(self):
      
        return bool(self.head is None)

    # Method to add data to the stack which will be added at the start
    def push(self, data):
        
        if self.head is None:
            self.head = Node(data)
        else:
            newnode = Node(data)
            newnode.next = self.head
            self.head = newnode

    # Remove element that is the current head (start of the stack)
    def pop(self):
        
        if self.isempty():
            return None
        # Removes the head node and makes the preceding element as the head
        poppednode = self.head
        self.head = self.head.next
        poppednode.next = None
        return poppednode.data

    # Prints the elements of stack
    def display(self):
        
        if self.isempty():
            return "Stack Underflow"
        iternode = self.head
        new_string = ''
        while iternode is not None:
            new_string += iternode.data
            iternode = iternode.next
        return new_string

    @staticmethod
    def isbalanced(str1):
        
        count = 0
        ans = False
        for i in str1:
            if i in ('(', '{', '['):
                count += 1
            elif i in (')', '}', ']'):
                count -= 1
            if count < 0:
                return ans
        if count == 0:
            return not ans
        return ans


# Driver code
if __name__ == "__main__":
    MyStack = Stack()

    MyStack.push(']')
    MyStack.push('}')
    MyStack.push(')')
    MyStack.push('(')
    MyStack.push(')')
    MyStack.push('(')
    MyStack.push('{')
    MyStack.push('[')

    # MyStack.pop()
    stack_data = MyStack.display()

    inp = stack_data
    print(inp, '-', MyStack.isbalanced(inp))

