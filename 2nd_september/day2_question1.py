class Node:

    def __init__(self, data):
        self.data = data
        self.next = None


class Linkedlist:
    

    def __init__(self):
        self.head = None

    @staticmethod
    def type_check(valueToCheck):
        
        try:
            # checks valueToCheck input is int and validate lower lowercap
            # if type(valueToCheck) is not int:
            if not isinstance(valueToCheck,int):
                raise ValueError
            return True
        except ValueError:
            return False

    def insert_at_beginning(self, data):
        
        new_node = Node(data)
        new_node.next = self.head
        self.head = new_node

    def insert_at_end(self, data):
        
        new_node = Node(data)
        temp = self.head
        while temp.next:
            temp = temp.next
        temp.next = new_node

    def insert_at_position(self, pos, data):
        
        # dealing with position argment type
        if self.type_check(pos):
            pass
        else:
            raise IndexError("Position argument should be a integer type")

        new_node = Node(data)
        temp = self.head
        i=1
        while i < (pos-1):
            temp = temp.next
            i+=1
        new_node.data = data
        new_node.next = temp.next
        temp.next = new_node

    def delete_at_beginning(self):
        
        # when linked list is empty
        if self.head is None:
            raise ValueError(
                "LinkedList is empty, doesn't have elements to remove")

        temp = self.head
        self.head = temp.next
        temp.next = None

    def delete_at_end(self):
       
        prev = self.head
        temp = self.head.next
        while temp.next is not None:
            temp = temp.next
            prev = prev.next
        prev.next = None

    def delete_at_position(self, pos):
      
        # dealing with position argment type
        if self.type_check(pos):
            pass
        else:
            raise IndexError("Position argument should be integer type")

        prev = self.head
        temp = self.head.next
        i=1
        while i < (pos-1):
            temp = temp.next
            prev = prev.next
            i+=1
        prev.next = temp.next

    def display(self):
        '''
                    prints the currently stored linked list

                    Raises
                    ------
                    ValueError
                            when linked list is empty

                    '''
        if self.head is None:
            raise ValueError("The Linked List is empty")

        # printing element wise
        temp = self.head
        while True:
            # when end of linked list is reached
            if temp is None:
                print(None)
                return
            print(temp.data, end=' -> ')
            temp = temp.next


# Driver Program
l = Linkedlist()
n = Node(1)
l.head = n
n1 = Node(2)
n.next = n1
n2 = Node(3)
n1.next = n2
n3 = Node(4)
n2.next = n3
l.insert_at_beginning(5)
l.insert_at_end(5)
l.insert_at_position(1, 5)
l.delete_at_beginning()
l.delete_at_end()
l.delete_at_position(5)
l.display()

